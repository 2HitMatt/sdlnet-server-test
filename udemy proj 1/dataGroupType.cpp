#include "dataGroupType.h"

void DataGroupType::saveRSDataGroupType(ofstream &file, DataGroupType dataGroupType){
	file << dataGroupType.groupName << endl;
	file << dataGroupType.dataType << endl;
	file << dataGroupType.singleItem << endl;
}
DataGroupType DataGroupType::loadRSDataGroupType(ifstream &file){
	DataGroupType dataGroupType;
	getline(file, dataGroupType.groupName);
	string buffer;
	getline(file, buffer);
	stringstream ss;
	ss << buffer;
	ss >> dataGroupType.dataType;
	getline(file, buffer);
	ss.clear();
	ss << buffer;
	ss>>dataGroupType.singleItem;


	return dataGroupType;
}