#ifndef ENTITYMANAGER
#define ENTITYMANAGER

#include "entity.h"
#include "hero.h"
#include "glob.h"
#include "grob.h"
#include "TimeFrameManager.h"

using namespace std;

struct EntityRegister{
	long entityId;
	string type;
};

class EntityManager
{
public:
	//for every entity created in a game, register their ids and types, so that if we need to rebuild them in a simulation we can
	//TODO - change over to hashMap
	list<EntityRegister> registeredEntities;

	//TODO move out if this is crucial :S:S:S:S:S
	TimeFrameManager tfm;

	//for sake of simulations, sometimes we need to rebuild an entity as they are deleted after use buuuuut we may resimulate back a few frames before deleting
	void recreateEntity(EntityFrame &ef){
		//look up this entity frame in the registeredEntities
		string type = "";
		for (list<EntityRegister>::iterator er = registeredEntities.begin(); er != registeredEntities.end(); er++)
		{
			if (er->entityId == ef.entityId)
			{
				type = er->type;
				break;
			}
		}
		Entity* entity;
		stringstream ss;
		ss << ef.data;
		if (type == "hero"){
			entity = new Hero();
			entity->readFromString(ss);
		}
		else if (type == "glob")
		{
			//ignore for now
		}
		else if (type == "grob")
		{
			//ignore for now
		}
	}

	void saveTimeFrame()
	{
		TimeFrame tf;
		tf.frameId = TimeFrame::getNextTimeFrameId();
		//TODO tf.gameState = 
		for (auto entity = Entity::entities.begin(); entity != Entity::entities.end(); entity++)
		{
			if ((*entity)->trackForSimulation){
				EntityFrame ef;
				ef.entityId = (*entity)->entityId;
				ef.active = (*entity)->active;
				stringstream ss;
				(*entity)->writeToString(ss);
				ef.data = ss.str();


				//is this guy registered?
				if (!(*entity)->registered)
				{
					//if not, better do it
					EntityRegister er;
					er.entityId = (*entity)->entityId;
					er.type = (*entity)->type;

					(*entity)->registered = true;

					registeredEntities.push_back(er);
				}

				//add this to the new current time frame
				tf.entityFrames.push_back(ef);
			}
			
		}
		//move this adding logic into time frame manager function
		tfm.timeFrames.push_back(tf);
		if (tfm.timeFrames.size() > tfm.maxCurrentTimeFrames)
			tfm.timeFrames.pop_front();
	}
};

#endif