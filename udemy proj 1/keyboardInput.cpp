#include "keyboardInput.h"

KeyboardInput::KeyboardInput(){
	UP = SDL_SCANCODE_UP; 
	DOWN = SDL_SCANCODE_DOWN; 
	LEFT = SDL_SCANCODE_LEFT; 
	RIGHT = SDL_SCANCODE_RIGHT;
	SLASH = SDL_SCANCODE_Z; 
	DASH = SDL_SCANCODE_X;
}

void KeyboardInput::update(SDL_Event* e){
	//button presses
	if (e->type == SDL_KEYDOWN){
		//DASH
		if (e->key.keysym.scancode == DASH)
		{
			hero->dash();
		}
		//SLASH
		if (e->key.keysym.scancode == SLASH)
		{
			hero->slash();
		}
		
	}

	//button holds
	//check for keys still being held
	const Uint8 *keystates = SDL_GetKeyboardState(NULL);
	//if hero not able to move or no directions pressed, then stop moving (still slide to a halt though)
	if ((hero->state != Hero::HERO_STATE_MOVE && hero->state != Hero::HERO_STATE_IDLE) || (!keystates[UP] && !keystates[DOWN] && !keystates[RIGHT] && !keystates[LEFT]))
	{
		hero->moving = false;
	}
	else
	{
		//ups
		if (keystates[UP])
		{
			//up right
			if (keystates[RIGHT])
				hero->move(270 + 45);
			else if (keystates[LEFT])
				hero->move(270 - 45);
			else
				hero->move(270);
		}
		//downs
		if (keystates[DOWN])
		{
			//up right
			if (keystates[RIGHT])
				hero->move(90 - 45);
			else if (keystates[LEFT])
				hero->move(90 + 45);
			else
				hero->move(90);
		}
		//left
		if (!keystates[UP] && !keystates[DOWN] && !keystates[RIGHT] && keystates[LEFT])
			hero->move(180);
		//right
		if (!keystates[UP] && !keystates[DOWN] && keystates[RIGHT] && !keystates[LEFT])
			hero->move(0);
	}

}