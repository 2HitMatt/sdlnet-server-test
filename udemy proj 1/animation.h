#ifndef Animation_header
#define Animation_header

#include <iostream>
#include <list>
#include "SDL.h"
#include "globals.h"
#include "Frame.h"

using namespace std;

class Animation
{
public:
	string name; //name of the animation, used to find and load up
	list<Frame> frames; //list of our frames
	
	//Functions:
	Animation(string name = "");
	
	int getNextFrameNumber(int frameNumber); //� returns the next Frame number in the list
	Frame* getNextFrame(Frame* frame);
	int getEndFrameNumber(); //- returns the final frame
	Frame* getFrame(int frameNumber); //get frame based on frame number. returns first frame is out of bounds

	//load- takes the currently open file stream, and also a list of what types of data groups a frame should have
	void loadAnimation(ifstream &file, list<DataGroupType> &groupTypes); //note: need to pass spritesheet through so frames can get the ref
};

#endif