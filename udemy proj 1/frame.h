#ifndef Frame_header
#define Frame_header

#include <iostream>
#include <list>
#include <fstream>
#include <sstream>
#include "SDL.h"
#include "drawing_functions.h"
#include "globals.h"
#include "groupBuilder.h"

using namespace std;

class Frame
{
public:
		//Attributes
		int frameNumber; //referred to as 'index' in the output files
		SDL_Rect clip; //its the region on the spritesheet where this frame is. SDL_Rect is a rectangle, 			//x,y,w,h
		float duration; //how long does this frame run for
		SDL_Point offSet; //pivot, x/y offset, whatever you wanna call it
		
		list<Group*> frameData; 

		void Draw(SDL_Texture *spriteSheetPtr, float x, float y);
		//save and load from a file, using the current ruleset
		void loadFrame(ifstream &file, list<DataGroupType> &groupTypes);
		
};


#endif