#ifndef GAME
#define GAME

#include "SDL_ttf.h"
#include "globals.h"
#include "hero.h"
#include "glob.h"
#include "grob.h"
#include "wall.h"
#include "keyboardInput.h"
#include "drawing_functions.h"
#include "soundManager.h"
#include "AssetManager.h"
#include "EntityManager.h"

class Game{
public:

	Mix_Music* song;

	AnimationSet* heroAnimSet;
	AnimationSet* globAnimSet;
	AnimationSet* grobAnimSet;
	AnimationSet* wallAnimSet;
	
	SDL_Texture* backgroundImage;
	SDL_Texture* splashImage;
	SDL_Texture* overlayImage;

	SDL_Texture* scoreTexture = NULL;

	Hero *hero;
	KeyboardInput heroInput;

	list<Entity*> enemies;
	list<Entity*> walls;

	bool splashShowing;
	float overlayTimer;


	//NEW
	EntityManager entityManager;
	//END NEW

	Game();
	~Game();

	void update();
	void draw();


};

#endif