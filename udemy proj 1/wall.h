#ifndef WALL
#define WALL

#include "Entity.h"

class Wall : public Entity{
public:

	Wall(AnimationSet *animSet);
	void update();
	void changeAnimation(int newState, bool resetFrameToBegging);
	virtual void updateCollisions(){
		//dont do anything? I'm a wall mofo, no moving me
	}
};

#endif