#include "game.h"

Game::Game(){

	string resPath = getResourcePath();
	backgroundImage = loadTexture(resPath+"map.png", Globals::renderer);
	splashImage = loadTexture(resPath + "cyborgtitle.png", Globals::renderer);
	overlayImage = loadTexture(resPath + "overlay.png", Globals::renderer);

	splashShowing = true;
	overlayTimer = 2;


	SoundManager::soundManager.loadSound("hit",resPath + "Randomize2.wav");
	SoundManager::soundManager.loadSound("enemyHit", resPath + "Hit_Hurt9.wav");
	SoundManager::soundManager.loadSound("swing", resPath + "Randomize21.wav");
	SoundManager::soundManager.loadSound("dash", resPath + "dash.wav");
	SoundManager::soundManager.loadSound("growl", resPath + "Randomize34.wav");
	SoundManager::soundManager.loadSound("enemyDie", resPath + "Randomize41.wav");

	song = Mix_LoadMUS(string(resPath + "Fatal Theory.wav").c_str()); //song by Ryan Beveridge
	if (song != NULL)
		Mix_PlayMusic(song, -1);

	//holds a list of group types. This list describes the types of groups of data our frames can have! :D
	list<DataGroupType> dataGroupTypes;

	//so what data can a frame have?
	//collision boxes, describes our physical shape for bumping into stuff
	DataGroupType colboxType;
	colboxType.groupName = "collisionBox";
	colboxType.dataType = DataGroupType::DATATYPE_BOX;

	//hitboxes, describes an area of where we intend to inflict damage, like where our sword is swinging
	DataGroupType hitboxType;
	hitboxType.groupName = "hitBox";
	hitboxType.dataType = DataGroupType::DATATYPE_BOX;

	//damage, how much our hitbox hurts
	DataGroupType dmgType;
	dmgType.groupName = "damage";
	dmgType.dataType = DataGroupType::DATATYPE_NUMBER;

	dataGroupTypes.push_back(colboxType);
	dataGroupTypes.push_back(hitboxType);
	dataGroupTypes.push_back(dmgType);

	heroAnimSet = new AnimationSet();
	heroAnimSet->loadAnimationSet("udemyCyborg.fdset", dataGroupTypes, true, 0,true);

	AssetManager::assetManager.heroAnimSet = heroAnimSet;

	wallAnimSet = new AnimationSet();
	wallAnimSet->loadAnimationSet("wall.fdset", dataGroupTypes);

	globAnimSet = new AnimationSet();
	globAnimSet->loadAnimationSet("glob.fdset", dataGroupTypes, true, 0,true);

	grobAnimSet = new AnimationSet();
	grobAnimSet->loadAnimationSet("grob.fdset", dataGroupTypes, true, 0, true);
	
	hero = new Hero();
	hero->x = Globals::ScreenWidth / 2;
	hero->y = Globals::ScreenHeight / 2;
	hero->invincibleTimer = 0;

	heroInput.hero = hero;

	Entity::entities.push_back(hero);


	//build all the walls for this game
	//first, tops n bottoms
	for (int i = 0; i < Globals::ScreenWidth / Globals::tileSize; i++){
		Wall* newWall = new Wall(wallAnimSet);
		newWall->x = i*Globals::tileSize+Globals::tileSize/2;
		newWall->y = Globals::tileSize / 2;
		walls.push_back(newWall);
		Entity::entities.push_back(newWall);
		

		//reuse pointer to build another wall bit
		newWall = new Wall(wallAnimSet);
		newWall->x = i*Globals::tileSize + Globals::tileSize / 2;
		newWall->y = Globals::ScreenHeight - Globals::tileSize / 2;
		walls.push_back(newWall);
		Entity::entities.push_back(newWall);
	}
	//then sides
	for (int i = 1; i < Globals::ScreenHeight / Globals::tileSize - 1; i++){
		Wall* newWall = new Wall(wallAnimSet);
		newWall->x = Globals::tileSize / 2;
		newWall->y = i*Globals::tileSize + Globals::tileSize / 2;
		walls.push_back(newWall);
		Entity::entities.push_back(newWall);

		//reuse pointer to build another wall bit
		newWall = new Wall(wallAnimSet);
		newWall->x = Globals::ScreenWidth - Globals::tileSize + Globals::tileSize / 2;
		newWall->y = i*Globals::tileSize + Globals::tileSize / 2;
		walls.push_back(newWall);
		Entity::entities.push_back(newWall);
	}
	
}
Game::~Game(){

	cleanup(backgroundImage);

	Mix_PauseMusic();
	Mix_FreeMusic(song);

	if (scoreTexture != NULL){
		cleanup(scoreTexture);
	}

	Entity::removeAllFromList(&Entity::entities, false);

	delete heroAnimSet;
	delete globAnimSet;
	delete grobAnimSet;
	delete wallAnimSet;

	delete hero;

	Entity::removeAllFromList(&enemies, true);
	Entity::removeAllFromList(&walls, true);
}

void Game::update(){

	int enemiesToBuild = 2;
	int enemiesBuilt = 0;
	float enemyBuildTimer = 1;
	bool quit = false;
	
	SDL_Event e;
	//setup my time controller before the game starts
	TimeController::timerController.reset();
	//Game Loop!
	while (!quit){
		TimeController::timerController.updateTime();

		//TODO - must somehow have alllll entities in the entities list before drawing
		Entity::removeInactiveEntitiesFromList(&Entity::entities, false);
		//remove dead enemies from the enemies list too
		Entity::removeInactiveEntitiesFromList(&enemies, true);//true, delete them (we dynamically built them here)
		
		
		//update our hero!
		//poll new inputs
		while (SDL_PollEvent(&e)){
			if (e.type == SDL_QUIT)
				quit = true;
			//Use number input to select which clip should be drawn
			if (e.type == SDL_KEYDOWN){
				switch (e.key.keysym.scancode){
				case SDL_SCANCODE_ESCAPE:
					quit = true;
					break;
				case SDL_SCANCODE_SPACE:
					if (splashShowing)
						splashShowing = false; //basically starts the game
					if (overlayTimer <= 0 && hero->hp < 1){
						//cleanup and restart the game
						enemiesToBuild = 2;
						enemiesBuilt = 0;
						enemyBuildTimer = 3;
						overlayTimer = 2;

						Glob::globsKilled = 0;

						if (scoreTexture != NULL){
							cleanup(scoreTexture);
							scoreTexture = NULL;
						}

						for (list<Entity*>::iterator enemy = enemies.begin(); enemy != enemies.end(); enemy++){
							(*enemy)->active = false;
						}
						hero->revive();
					}
				}
			}

			heroInput.update(&e);
		}
		if (hero->hp < 1 && overlayTimer > 0)
		{
			overlayTimer -= TimeController::timerController.dT;
		}

		//update all entities
		for (list<Entity*>::iterator entity = Entity::entities.begin(); entity != Entity::entities.end(); entity++)
		{
			//calling update here to save a loop
			(*entity)->update();
		}

		//NEW!!!!
		entityManager.saveTimeFrame();

		//generate new enemies
		if (hero->hp > 0 && !splashShowing){
			if (enemiesToBuild == enemiesBuilt){
				enemiesToBuild = enemiesToBuild * 2;
				enemiesBuilt = 0;
				enemyBuildTimer = 4; //bigger delay between waves. creates flow. player needs minor resting time
			}
			enemyBuildTimer -= TimeController::timerController.dT;
			if (enemyBuildTimer <= 0 && enemiesBuilt < enemiesToBuild && enemies.size() < 10){
			/*	Glob *enemy = new Glob(globAnimSet);
				enemy->x = getRandomNumber(Globals::ScreenWidth - (2 * Globals::tileSize) - 32) + Globals::tileSize + 16; //random x value between our walls
				enemy->y = getRandomNumber(Globals::ScreenHeight - (2 * Globals::tileSize) - 32) + Globals::tileSize + 16; //random x value between our walls
				enemy->invincibleTimer = 0.01;

				enemies.push_back(enemy);
				Entity::entities.push_back(enemy);
				

				enemiesBuilt++;

				if (enemiesBuilt % 5 == 0)
				{
					Grob *grob = new Grob(grobAnimSet);
					grob->x = getRandomNumber(Globals::ScreenWidth - (2 * Globals::tileSize) - 32) + Globals::tileSize + 16; //random x value between our walls
					grob->y = getRandomNumber(Globals::ScreenHeight - (2 * Globals::tileSize) - 32) + Globals::tileSize + 16; //random x value between our walls
					grob->invincibleTimer = 0.01;

					enemies.push_back(grob);
					Entity::entities.push_back(grob);
				}
				*/
			}
		}

		draw();

	}

}
void Game::draw(){
	
	//clear the screen
	SDL_SetRenderDrawColor(Globals::renderer, 145, 133, 129, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(Globals::renderer);

	if (splashShowing)
	{
		renderTexture(splashImage, Globals::renderer, 0, 0);

	}
	else{

		//draw background
		renderTexture(backgroundImage, Globals::renderer, 0, 0);

		//draw all entities
		Entity::entities.sort(Entity::EntityCompare);
		for (list<Entity*>::iterator entity = Entity::entities.begin(); entity != Entity::entities.end(); entity++)
		{
			//calling update here to save a loop
			(*entity)->draw();
		}

		if (overlayTimer <= 0 && hero->hp < 1){
			renderTexture(overlayImage, Globals::renderer, 0, 0);

			if (scoreTexture == NULL){
				SDL_Color color = { 255, 255, 255, 255 }; //white

				stringstream ss;
				ss << "Enemies dispatched: " << Glob::globsKilled + Grob::grobsKilled;

				string resPath = getResourcePath();
				scoreTexture = renderText(ss.str(), resPath + "vermin_vibes_1989.ttf", color, 30, Globals::renderer);
			}
			renderTexture(scoreTexture, Globals::renderer, 20, 50);
		}

	}

	//After we're done drawing to the renderer, we should show it to the screen :D
	SDL_RenderPresent(Globals::renderer);
}