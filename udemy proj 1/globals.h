#ifndef GLOBALS_H
#define GLOBALS_H

#include <string>
#include <iostream>
#include <SDL.h>
#include "randomNumber.h"

using namespace std;

//http://www.learncpp.com/cpp-tutorial/811-static-member-variables/
class Globals
{
public:
	//math helpers
	static const float PI;

	//useful for dev people
	static bool debugging;

	//sdl related
	static int tileSize;
	static int ScreenWidth, ScreenHeight, ScreenScale;
	static SDL_Renderer* renderer;



	//move later to somewhere more appropriate
	

	//other helpers
	/**cuts off the extra info. e.g hitbox: 2 23 23 23, it cuts off the 'hitbox: ' part and just leaves the numbers*/
	static string clipOffDataHeader(string str);
	
};

#endif