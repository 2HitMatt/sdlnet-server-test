#include "livingEntity.h"

void LivingEntity::updateHitBox(){
	//assume damage 0 by default
	damage = 0;
	
	//update this now for damage checking

	GroupBox* colBoxes = (GroupBox*)GroupBuilder::findGroupByName("hitBox", currentFrame->frameData);
	if (colBoxes != NULL && colBoxes->numberOfDataInGroup() > 0)
	{
		//update hitbox
		SDL_Rect hb = colBoxes->data.front();
		hitBox.x = x - currentFrame->offSet.x + hb.x;
		hitBox.y = y - currentFrame->offSet.y + hb.y;
		hitBox.w = hb.w;
		hitBox.h = hb.h;
		//update damage amount
		GroupNumber* damages = (GroupNumber*)GroupBuilder::findGroupByName("damage", currentFrame->frameData);
		if (damages != NULL && damages->numberOfDataInGroup() > 0)
		{
			damage = damages->data.front();
		}
	}
}

void LivingEntity::updateInvincibleTimer(){
	if (invincibleTimer > 0)
		invincibleTimer -= TimeController::timerController.dT;
}

void LivingEntity::writeToString(stringstream& data){
	Entity::writeToString(data);
	data << " " << hp << " " << hpMax << " " << damage << " " << hitBox.x << " " << hitBox.y << " " << hitBox.w << " " << hitBox.h;
}

void LivingEntity::readFromString(stringstream& data){
	Entity::readFromString(data);
	data >> hp >> hpMax >> damage >> hitBox.x >> hitBox.y >> hitBox.w >> hitBox.h;
}

void LivingEntity::draw(){
	if (currentFrame != NULL && active){
		if (invincibleTimer > 0 && animSet->whiteSpriteSheet != NULL)
			currentFrame->Draw(animSet->whiteSpriteSheet, x, y);
		else
			currentFrame->Draw(animSet->spriteSheet, x, y);
	}
	if (solid && Globals::debugging)
	{
		SDL_SetRenderDrawColor(Globals::renderer, 0, 0, 255, 255);
		SDL_RenderDrawRect(Globals::renderer, &lastCollisionBox);

		SDL_SetRenderDrawColor(Globals::renderer, 255, 0, 0, 255);
		SDL_RenderDrawRect(Globals::renderer, &collisionBox);

		SDL_SetRenderDrawColor(Globals::renderer, 255, 0, 255, 255);
		SDL_RenderDrawRect(Globals::renderer, &hitBox);
	}
}