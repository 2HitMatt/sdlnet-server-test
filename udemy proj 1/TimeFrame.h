#ifndef TIMEFRAME
#define TIMEFRAME

#include <iostream>
#include <string>
#include <list>

using namespace std;

//most basic elements of an entity
struct EntityFrame{
	long entityId;
	bool active = true;
	//all other data stored in a string (x,y, moving etc)
	string data;
};

class TimeFrame
{
public:
	long frameId;
	int gameState;
	list<EntityFrame> entityFrames;

	static long currentFrameId;
	static long getNextTimeFrameId();

};

#endif