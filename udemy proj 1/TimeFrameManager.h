#ifndef TIMEFRAMEMANAGER
#define TIMEFRAMEMANAGER

#include "TimeFrame.h"

using namespace std;

class TimeFrameManager{
public:
	//update time interval( 1 = 1 second)
	float timeFrameInterval = 0.1; //every 100ms

	//the number of current time frames
	int maxCurrentTimeFrames = 3 * (1.0 / timeFrameInterval); //3 seconds worth of 100ms's

	//this is just the current set of frames describing the game world for the last x many seconds
	list<TimeFrame> timeFrames;


};

#endif