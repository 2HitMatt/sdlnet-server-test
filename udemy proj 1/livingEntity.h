#ifndef LIVINGENTITY
#define LIVINGENTITY

#include "Entity.h"

//abstract also
class LivingEntity : public Entity
{
public:
	//things that live have health and potentially cause pain
	int hp, hpMax;
	int damage = 0;
	SDL_Rect hitBox; //the thing that we hit with


	float invincibleTimer = 0;

	virtual void updateHitBox();
	virtual void updateDamages() = 0; //how we get damaged by other things is up to each class
	virtual void die()=0;
	virtual void updateInvincibleTimer();

	virtual void writeToString(stringstream& data);
	virtual void readFromString(stringstream& data);

	void draw();

};

#endif