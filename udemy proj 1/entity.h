#ifndef ENTITY
#define ENTITY

#include "globals.h"
#include "TimeController.h"
#include "animationSet.h"
#include "AssetManager.h"

//abstract class. Gonna use it for POLYMORPHISM :D. This is our base for everything in our little world
class Entity{
public:
	//reference constants
	static const int DIR_UP;
	static const int DIR_DOWN;
	static const int DIR_LEFT;
	static const int DIR_RIGHT;
	static const int DIR_NONE;

	//every entity gets an id, yo
	long entityId;
	bool registered = false;
	bool trackForSimulation = false;

	//all entities keep track of a state. This is a quick label to say what the entity is up to
	int state;

	//from hero to ui, still need to position stuff and turn it off and on(active)
	float x, y;
	int direction;
	bool solid = true; //is this thing a hard object or can things pass through it ( most are hard by default)
	bool collideWithSolids = true; //sometimes we want to be solid, but we ourselves, pass through other solids
	bool active = true; //entity turned off or on
	string type = "entity"; //what type of thing is this entity e.g hero, enemy, wall etc
	bool moving; //yes or no
	float angle; //angle to move hero
	float moveSpeed; //speed to move hero in direction
	float moveSpeedMax;
	float slideAngle; //direction pushed in
	float slideAmount; //amount of push in that direction
	float moveLerp = 4;
	float totalXMove, totalYMove; //keeps track of movement amount this turn. used just in case we want to retract our move

	SDL_Rect collisionBox;//a region describing the solid parts of the entity
	SDL_Rect lastCollisionBox; //the last hitbox used. This helps work out collisions based on movement/animation
	int collisionBoxW, collisionBoxH; //our default values to return to
	float collisionBoxYOffset; //from entities y, how should I move the collision box?


	//everything in the world has animations and stuff right?
	AnimationSet *animSet;
	Animation *currentAnim;
	Frame *currentFrame;
	float frameTimer;

	//VIRTUAL FUNCTIONS
	virtual void update();
	virtual void draw();

	virtual void move(float angle);
	virtual void slide(float angle, float amount);
	virtual void updateMovement();
	virtual void updateCollisionBox();

	virtual void changeAnimation(int newState, bool resetFrameToBegging) = 0;
	virtual void updateCollisions(); //how do we bump into the world around us is different per class too

	virtual void writeToString(stringstream& data);
	virtual void readFromString(stringstream& data);

	//helper functions for the class
	static float distanceBetweenTwoRects(SDL_Rect &r1, SDL_Rect &r2);
	static float distanceBetweenTwoEntities(Entity *e1, Entity *e2);
	static float angleBetweenTwoEntities(Entity *e1, Entity *e2);
	static bool checkCollision(SDL_Rect cbox1, SDL_Rect cbox2);
	static int angleToDirection(float angle);
	static int getCollisionDirection(SDL_Rect box, float x, float y);
	static float angleBetweenTwoPoints(float cx1, float cy1, float cx2, float cy2);
	static float angleBetweenTwoSDLRect(SDL_Rect &col1, SDL_Rect &col2);

	//static, means there is only one entities list ever made in the whole program. 
	//It belongs to the class as a whole, not a single object built from a class
	//We will chuck allll of our entities into this bad boy list, so we can sort them and cascade over them easily
	//NOTE: this is a list of pointers, we won't delete entities in this list, refer to each entities list for deleting purposes
	static list<Entity*> entities;
	//static list<Entity*> otherEntities; //used for special effects etc

	//this function helps us sort our entities in the entities list. We're going to sort based on y (or depth into the screen, since its a top downy game)
	static bool EntityCompare(const Entity * const & a, const Entity * const & b);
	//this function helps erase inactive entities from a list
	static void removeInactiveEntitiesFromList(list<Entity*> *entityList, bool deleteEntities);
	//helps empty a list
	static void removeAllFromList(list<Entity*> *entityList, bool deleteEntities);

	//ID management
	static long currentEntityId ;
	static long getNextEntityId();
};

#endif