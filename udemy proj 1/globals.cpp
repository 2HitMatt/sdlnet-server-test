#include "globals.h"


const float Globals::PI = 3.14159;

bool Globals::debugging = false;

SDL_Renderer* Globals::renderer = NULL;
int Globals::tileSize = 32;
int Globals::ScreenWidth = 640, Globals::ScreenHeight = 352, Globals::ScreenScale = 2;


string Globals::clipOffDataHeader(string str){
	int pos = str.find(":", 0);
	if (pos != -1){
		str = str.substr(pos + 1, str.length() - pos + 2);
	}
	return str;
}