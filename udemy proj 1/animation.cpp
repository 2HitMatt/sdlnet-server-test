#include "Animation.h"

//Constructor() - takes a string name, and when we get to it, a filestream to read in a bunch of frame data.We'll finish this one after we make AnimationSet
Animation::Animation(string name)
{
	this->name = name;
	//name; //name of the animation, used to find and load up
	//int numberOfFrames = 0
	//list<Frame> frames; //list of our frames
}

//void Draw(bool direction, float x, float y); //� calls the current frames draw
int Animation::getNextFrameNumber(int frameNumber)
{

	//int counter = 0;

	if (frameNumber + 1 < frames.size())
	{
		return frameNumber+1;

	}
	else
	{
		return 0;
	}

}

Frame* Animation::getNextFrame(Frame *frame)
{
	return getFrame(getNextFrameNumber(frame->frameNumber));
}

int Animation::getEndFrameNumber()
{
	return frames.size() - 1;

}

Frame* Animation::getFrame(int frameNumber)
{
	if (frames.size() == 0)
		return NULL;
	
	list<Frame>::iterator i = frames.begin();
	//Frame* frame = &frames.front();

	int counter = 0;

	for (counter = 0; counter < frameNumber && counter < frames.size()-1;counter++)
	{
		i++;
		//frame = getNextFrame(frame);
	

	}

	//return frame;
	return &*i;
}

void Animation::loadAnimation(ifstream &file, list<DataGroupType> &groupTypes){
	getline(file, name);
	string buffer;
	getline(file, buffer);
	stringstream ss;
	buffer = Globals::clipOffDataHeader(buffer);
	ss << buffer;
	int numberOfFrames;
	ss >> numberOfFrames;
	for (int i = 0; i < numberOfFrames; i++){
		Frame newFrame;
		newFrame.loadFrame(file, groupTypes);
		frames.push_back(newFrame);
	}


}
