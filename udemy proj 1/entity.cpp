#include "entity.h"

//statics/constants
const int Entity::DIR_UP = 0;
const int Entity::DIR_DOWN = 1;
const int Entity::DIR_LEFT = 2;
const int Entity::DIR_RIGHT = 3;
const int Entity::DIR_NONE = -1;

list<Entity *> Entity::entities;

void Entity::update(){
	//override me if you want logic to happen
}

void Entity::draw(){
	//override me if you want something more specific to happen
	//renderTexture(animSet->frameSheet, Globals::renderer, x - currentFrame->xOffset, y - currentFrame->yOffset, &currentFrame->clip);
	if (currentFrame != NULL && active){
		currentFrame->Draw(animSet->spriteSheet, x, y);
	}
	if (solid && Globals::debugging)
	{
		SDL_SetRenderDrawColor(Globals::renderer, 255, 0, 0, 255);
		SDL_RenderDrawRect(Globals::renderer, &collisionBox);
	}
}


void Entity::move(float angle)
{
	moving = true;
	moveSpeed = moveSpeedMax;
	this->angle = angle;

	int newDirection = angleToDirection(angle);
	//if direction changed, update animations
	if (direction != newDirection){
		direction = newDirection;
		changeAnimation(state, false);
	}

}

void Entity::slide(float angle, float amount)
{
	slideAngle = angle;
	slideAmount = amount;
}

void Entity::updateMovement(){

	//setup collisionBoxes for where we are now
	updateCollisionBox();
	//and store it inside of lastCollisionBox (used later, look down
	lastCollisionBox = collisionBox;

	//reset these back to 0
	totalXMove = 0;
	totalYMove = 0;

	if (moveSpeed > 0)
	{
		float movedist = moveSpeed*(TimeController::timerController.dT)*moveLerp;

		if (movedist > 0)
		{
			float xMove = movedist*(cos(angle*Globals::PI / 180));
			float yMove = movedist*(sin(angle*Globals::PI / 180));

			x += xMove;
			y += yMove;

			totalXMove = xMove;
			totalYMove = yMove;

			if (!moving)
				moveSpeed -= movedist;
		}
		else if (!moving)
		{
			moveSpeed = 0;
		}
	}
	if (slideAmount > 0){
		float slideDist = slideAmount*TimeController::timerController.dT*moveLerp;
		if (slideDist > 0){
			float xMove = slideDist*(cos(slideAngle*Globals::PI / 180));
			float yMove = slideDist*(sin(slideAngle*Globals::PI / 180));

			x += xMove;
			y += yMove;

			totalXMove += xMove;
			totalYMove += yMove;
			slideAmount -= slideDist;
		}
		else
		{
			slideAmount = 0;
		}
		
	}
	//now, we get the new collisionbox for where we have moved to
	updateCollisionBox();
	//but what if the collisionBox is small and we are moving so fast that we jump enough pixels to hop over an obstacles collisionbox?
	//we combine the 2 boxes. Where we are now and where we were
	SDL_UnionRect(&collisionBox, &lastCollisionBox, &collisionBox);

}

void Entity::updateCollisionBox(){
	//update this now for collision checking
	/*
	GroupBox* colBoxes = (GroupBox*)GroupBuilder::findGroupByName("collisionBox", currentFrame->frameData);
	if (colBoxes != NULL && colBoxes->numberOfDataInGroup() > 0)
	{
		//get the basic collisionbox details from the frame data
		SDL_Rect cb = colBoxes->data.front();
		//update it to be based on where the character is/offset
		cb.x = x - currentFrame->offSet.x + cb.x;
		cb.y = y - currentFrame->offSet.y + cb.y;
		
		//set our collisionBox
		collisionBox = cb;

		//we should let the entity decide really
		//solid = true;
	}
	else{
		solid = false;
	}
	*/
	collisionBox.x = x - collisionBox.w / 2;
	collisionBox.y = y + collisionBoxYOffset;
	collisionBox.w = collisionBoxW;
	collisionBox.h = collisionBoxH;
}

void Entity::updateCollisions(){
	if (active && collideWithSolids && (moveSpeed > 0 || slideAmount > 0)){
		list<Entity*> collisions;
		
		for (auto entity = Entity::entities.begin(); entity != Entity::entities.end(); entity++)
		{
			if ((*entity)->active && (*entity)->type != this->type && (*entity)->solid && Entity::checkCollision(collisionBox, (*entity)->collisionBox)){
				
				moveSpeed = 0;
				moving = false;

				collisions.push_back(*entity);				
			}
		}
		if (collisions.size() > 0){
			
			updateCollisionBox();

			//x -= totalXMove;
			//y -= totalYMove;

			//MULTISAMPLE CODE. Why? Because I gotta! :P

			//firsts, what the biggest length OUR collisionbox and then divide by 2
			float boxHalfSize = 0;
			if (collisionBox.w < collisionBox.h)
				boxHalfSize = collisionBox.w / 4;
			else
				boxHalfSize = collisionBox.h / 4;

			//ok, so the above code before just checked for any possible colliding entities on the path
			//now we are going to actually step by step move towards our destination and check collisions as we move through time
			SDL_Rect sampleBox = lastCollisionBox;
			float movementAngle = Entity::angleBetweenTwoSDLRect(lastCollisionBox, collisionBox);

			bool foundCollision = false;
			while (!foundCollision){
				//check for collisions, take a step if we haven't found any


				SDL_Rect intersection;
				for (auto entity = collisions.begin(); entity != collisions.end(); entity++)
				{
					if (SDL_IntersectRect(&sampleBox, &(*entity)->collisionBox, &intersection)){
						foundCollision = true;
						slideAngle = angleBetweenTwoEntities((*entity), this);

						//do something about it?
						//if (lastCollisionBox.x + lastCollisionBox.w / 2 != (*entity)->collisionBox.x + (*entity)->collisionBox.w / 2){
						if (intersection.w < intersection.h){
							if (lastCollisionBox.x + lastCollisionBox.w / 2 < (*entity)->collisionBox.x + (*entity)->collisionBox.w / 2)
								sampleBox.x -= intersection.w;
							else
								sampleBox.x += intersection.w;
						}
						//if (lastCollisionBox.y + lastCollisionBox.h / 2 != (*entity)->collisionBox.y + (*entity)->collisionBox.h / 2){
						else{	
						if (lastCollisionBox.y + lastCollisionBox.h / 2 < (*entity)->collisionBox.y + (*entity)->collisionBox.h / 2)
								sampleBox.y -= intersection.h;
							else
								sampleBox.y += intersection.h;
							
						}
					}
				}
				//break out of the collision early
				if (foundCollision ||(sampleBox.x == collisionBox.x && sampleBox.y == collisionBox.y))
					break;

				if (distanceBetweenTwoRects(sampleBox, collisionBox) > boxHalfSize){
					float xMove = boxHalfSize*(cos(movementAngle*Globals::PI / 180));
					float yMove = boxHalfSize*(sin(movementAngle*Globals::PI / 180));

					sampleBox.x += xMove;
					sampleBox.y += yMove;
				}
				else
				{
					sampleBox = collisionBox;
				}


			} 
			
			if (foundCollision){
				//move us to wherever sampleBox is now
				slideAmount = slideAmount / 2;
				x = sampleBox.x + sampleBox.w / 2;
				y = sampleBox.y - collisionBoxYOffset;
			}
			
		}
		
	}
	updateCollisionBox();
}


void Entity::writeToString(stringstream& data){
	/*ef.state = (*entity)->state;
			ef.x = (*entity)->x;
			ef.y = (*entity)->y;
			ef.angle = (*entity)->angle;
			ef.direction = (*entity)->direction;
			ef.moveSpeed = (*entity)->moveSpeed;
			ef.moving = (*entity)->moving;
			ef.slideAmount = (*entity)->slideAmount;
			ef.slideAngle = (*entity)->slideAngle;*/
	string currentAnimStr = "NULL";
	if (currentAnim != NULL)
		currentAnimStr = currentAnim->name;
	int currentFrameNo = 0;
	if (currentFrame != NULL)
		currentFrameNo = currentFrame->frameNumber;

	data <<entityId<<" "<<type<<" "<<active<<" "<< state << " " << x << " " << y << " "<<currentAnimStr<<" "<<currentFrameNo<<" " << angle << " " << direction << " " << moveSpeed << " " << moving << " " << slideAmount << " " << slideAngle;
}

void Entity::readFromString(stringstream& data){
	string currentAnimStr;
	int currentFrameNo;
	data >> entityId >> type >> active >> state >> x >> y >> currentAnimStr>> currentFrameNo>> angle >> direction >> moveSpeed >> moving >> slideAmount >> slideAngle;
	//TODO possibly more or less work here ;)
	if (currentAnimStr != "NULL")
	{
		if (currentAnim == NULL || (currentAnim != NULL && currentAnim->name != currentAnimStr))
		{
			currentAnim = animSet->getAnimation(currentAnimStr);
			currentFrame = currentAnim->getFrame(currentFrameNo);
		}

	}	
	

}

//help functions
float Entity::distanceBetweenTwoRects(SDL_Rect &r1, SDL_Rect &r2)
{
	SDL_Point e1, e2;
	e1.x = r1.x + r1.w / 2;
	e1.y = r1.y + r1.h / 2;

	e2.x = r2.x + r2.w / 2;
	e2.y = r2.y + r2.h / 2;

	float d = abs(sqrt((e2.x - e1.x)*(e2.x - e1.x) + (e2.y - e1.y)*(e2.y - e1.y)));
	return d;
}

float Entity::distanceBetweenTwoEntities(Entity *e1, Entity *e2)
{
	float d = abs(sqrt((e2->x - e1->x)*(e2->x - e1->x) + (e2->y - e1->y)*(e2->y - e1->y)));
	return d;
}

float Entity::angleBetweenTwoEntities(Entity *e1, Entity *e2)
{
	float dx, dy;
	float x1 = e1->x, y1 = e1->y, x2 = e2->x, y2 = e2->y;
	if (e1->solid){
		x1 = e1->collisionBox.x + e1->collisionBox.w / 2;
		y1 = e1->collisionBox.y + e1->collisionBox.h / 2;
	}
	if (e2->solid){
		x2 = e2->collisionBox.x + e2->collisionBox.w / 2;
		y2 = e2->collisionBox.y + e2->collisionBox.h / 2;
	}

	dx = x2 - x1;
	dy = y2 - y1;


	return atan2(dy, dx) * 180 / Globals::PI;
}

bool Entity::checkCollision(SDL_Rect cbox1, SDL_Rect cbox2)
{
	SDL_Rect inter;
	if (SDL_IntersectRect(&cbox1, &cbox2, &inter))
	{
		return true;
	}
	else{
		return false;
	}
	//horizontal first		
		//otherwise, false
	return false;
}

int Entity::angleToDirection(float angle){
	if ((angle >= 0 && angle <= 45) || (angle >= 270 + 45 && angle <= 360))
	{
		return DIR_RIGHT;
	}
	else if (angle >= 45 && angle <= 90 + 45)
	{
		return DIR_DOWN;
	}
	else if (angle >= 90 + 45 && angle <= 180 + 45)
	{
		return DIR_LEFT;
	}
	else
	{
		return DIR_UP;
	}
}

int Entity::getCollisionDirection(SDL_Rect box, float x, float y){
	if (x >= box.x + box.w && y >= box.y && y <= box.y + box.h)
		return DIR_LEFT;
	if (x <= box.x && y >= box.y && y <= box.y + box.h)
		return DIR_RIGHT;
	if (y >= box.y + box.h && x >= box.x && x <= box.x + box.w)
		return DIR_DOWN;
	if (y <= box.y && x >= box.x && x <= box.x + box.w)
		return DIR_UP;

	return DIR_NONE;
}

float Entity::angleBetweenTwoPoints(float cx1, float cy1, float cx2, float cy2)
{

	//angle math stuff
	float dx = cx2 - cx1;
	float dy = cy2 - cy1;

	return atan2(dy, dx) * 180 / Globals::PI;
}

float Entity::angleBetweenTwoSDLRect(SDL_Rect &col1, SDL_Rect &col2)
{
	//get the centre x and y of each rectangle
	float cx1 = col1.x + (col1.w / 2);
	float cy1 = col1.y + (col1.h / 2);
	float cx2 = col2.x + (col2.w / 2);
	float cy2 = col2.y + (col2.h / 2);

	//angle math stuff
	/*float dx = cx2 - cx1;
	float dy = cy2 - cy1;

	return atan2(dy, dx) * 180 / Globals::PI;*/
	return angleBetweenTwoPoints(cx1, cy1, cx2, cy2);
}


bool Entity::EntityCompare(const Entity * const & a, const Entity * const & b)
{
	if (a != 0 && b != 0)
	{
		return a->y < b->y;
	}
	else{
		return false;
	}

}


void Entity::removeInactiveEntitiesFromList(list<Entity*> *entityList, bool deleteEntities){
	for (list<Entity *>::iterator i = entityList->begin(); i != entityList->end();)
	{
		if (!(*i)->active)
		{
			if (deleteEntities)
				delete (*i);
			i = entityList->erase(i);
		}
		else
		{
			++i;
		}
	}
}

void Entity::removeAllFromList(list<Entity*> *entityList, bool deleteEntities){
	 for (list<Entity *>::iterator i = entityList->begin(); i != entityList->end();)
	{
		if (deleteEntities)
			delete (*i);
		i = entityList->erase(i);
		
	}
}

long Entity::currentEntityId = 0;
long Entity::getNextEntityId(){
	currentEntityId++;
	return currentEntityId;
}