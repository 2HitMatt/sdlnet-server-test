#include "glob.h"

const string Glob::GLOB_ANIM_UP = "up";
const string Glob::GLOB_ANIM_DOWN = "down";
const string Glob::GLOB_ANIM_LEFT = "left";
const string Glob::GLOB_ANIM_RIGHT = "right";
const string Glob::GLOB_ANIM_IDLE_UP = "idleUp";
const string Glob::GLOB_ANIM_IDLE_DOWN = "idleDown";
const string Glob::GLOB_ANIM_IDLE_LEFT = "idleLeft";
const string Glob::GLOB_ANIM_IDLE_RIGHT = "idleRight";
const string Glob::GLOB_ANIM_ATTACK_UP = "attackUp";
const string Glob::GLOB_ANIM_ATTACK_DOWN = "attackDown";
const string Glob::GLOB_ANIM_ATTACK_LEFT = "attackLeft";
const string Glob::GLOB_ANIM_ATTACK_RIGHT = "attackRight";
const string Glob::GLOB_ANIM_TELEGRAPH_UP = "telegraphUp";
const string Glob::GLOB_ANIM_TELEGRAPH_DOWN = "telegraphDown";
const string Glob::GLOB_ANIM_TELEGRAPH_LEFT = "telegraphLeft";
const string Glob::GLOB_ANIM_TELEGRAPH_RIGHT = "telegraphRight";
const string Glob::GLOB_ANIM_DIE = "die";

const int Glob::GLOB_STATE_IDLE = 0;
const int Glob::GLOB_STATE_MOVE = 1;
const int Glob::GLOB_STATE_ATTACK = 2;
const int Glob::GLOB_STATE_TELEGRAPH = 3;
const int Glob::GLOB_STATE_DEAD = 4;

const int Glob::GLOB_AI_NORMAL = 0;
const int Glob::GLOB_AI_CHASE = 1;

int Glob::globsKilled = 0;

//main functions
Glob::Glob(AnimationSet *animSet){

	this->animSet = animSet;

	type = "enemy";

	//setup defaults
	x = Globals::ScreenWidth / 2;
	y = Globals::ScreenHeight / 2;
	moveSpeed = 0;
	moveSpeedMax = 20;
	hp = hpMax = 10+(rand()%20);
	damage = 0;
	collisionBoxW = 18;
	collisionBoxH = 16;
	collisionBox.w = collisionBoxW;
	collisionBox.h = collisionBoxH;

	collisionBoxYOffset = -14;

	direction = DIR_DOWN;

	changeAnimation(GLOB_STATE_IDLE, true);

	updateCollisionBox();
}
void Glob::update(){
	//check if died
	if (hp < 1 && state != GLOB_STATE_DEAD){
		changeAnimation(GLOB_STATE_DEAD, true);
		moving = false;
		die();
	}

	think();

	//update collision boxes
	updateCollisionBox();
	//update movement/input
	updateMovement();

	//bump into stuff
	updateCollisions();

	//only care about damage hitboxes after we're landed on a final spot in our code
	updateHitBox();
	updateDamages();

	//update animations
	updateAnimation();
	//update timers and things
	updateInvincibleTimer();
}

void Glob::think(){
	if (state == GLOB_STATE_IDLE || state == GLOB_STATE_MOVE){
		thinkTimer -= TimeController::timerController.dT;

		if (thinkTimer <= 0){
			//reset timer
			thinkTimer = rand() % 5;
			int action = rand() % 10;
			//be idle
			if (action < 3){
				moving = false;
				aiState = GLOB_AI_NORMAL;
				changeAnimation(GLOB_STATE_IDLE, true);
			}
			else{
				findNearestTarget();

				if (target != NULL && target->hp > 0){
					float dist = Entity::distanceBetweenTwoEntities(this, target);

					//if in range to attack, then lets do it
					if (dist < 100){
						telegraph();//telegraph our attack first, so players have a chance to dodge it
						aiState = GLOB_AI_NORMAL;
						
					}
					else
					{
						//otherwise move towards player to get into range
						aiState = GLOB_AI_CHASE;
						moving = true;
						changeAnimation(GLOB_STATE_MOVE, state != GLOB_STATE_MOVE);
					}
				}
				else
				{
					//otherwise just be happy and idle :/
					moving = false;
					aiState = GLOB_AI_NORMAL;
					changeAnimation(GLOB_STATE_IDLE, true);
				}

			}
		}
	}
	//if chasing a target, then do that
	if (aiState == GLOB_AI_CHASE && hp > 0 && active){
		//finds the angle between this enemy and its target
		angle = Entity::angleBetweenTwoEntities(this, target);
		//move enemy in this direction
		move(angle);
	}
}

void Glob::telegraph(){
	moving = false;
	frameTimer = 0;
	changeAnimation(GLOB_STATE_TELEGRAPH , true);
}
void Glob::attack(){
	moving = false;
	frameTimer = 0;
	slideAmount = 100;
	slideAngle = angle;
	changeAnimation(GLOB_STATE_ATTACK, true);
}
void Glob::die(){
	moving = false;
	state = GLOB_STATE_DEAD;
	changeAnimation(state, true);
	SoundManager::soundManager.playSound("enemyDie");

	//add to our score count
	Glob::globsKilled++;
}


void Glob::findNearestTarget()
{
	target = NULL;
	//find closest target (we only have 1 hero, but if we had multiple players or NPC's this will help)
	for (auto entity = Entity::entities.begin(); entity != Entity::entities.end(); entity++){
		if ((*entity)->type == "hero" && (*entity)->active){
			if (target == NULL){
				target = (LivingEntity*)(*entity); //cast entity to livingEntity NOTE! only do this if you know its a living entity!!!
			}
			else{
				//if other hero is closer then the current target, switch
				if (Entity::distanceBetweenTwoEntities(this, (*entity)) < Entity::distanceBetweenTwoEntities(this, target)){
					target = (LivingEntity*)(*entity);
				}
			}
		}
	}
}
void Glob::changeAnimation(int newState, bool resetFrameToBegging){
	state = newState;
	if (state == GLOB_STATE_IDLE)
	{
		if (direction == DIR_DOWN)
			currentAnim = animSet->getAnimation(GLOB_ANIM_IDLE_DOWN);
		if (direction == DIR_UP)
			currentAnim = animSet->getAnimation(GLOB_ANIM_IDLE_UP);
		if (direction == DIR_LEFT)
			currentAnim = animSet->getAnimation(GLOB_ANIM_IDLE_LEFT);
		if (direction == DIR_RIGHT)
			currentAnim = animSet->getAnimation(GLOB_ANIM_IDLE_RIGHT);
	}
	else if (state == GLOB_STATE_MOVE)
	{
		if (direction == DIR_DOWN)
			currentAnim = animSet->getAnimation(GLOB_ANIM_DOWN);
		if (direction == DIR_UP)
			currentAnim = animSet->getAnimation(GLOB_ANIM_UP);
		if (direction == DIR_LEFT)
			currentAnim = animSet->getAnimation(GLOB_ANIM_LEFT);
		if (direction == DIR_RIGHT)
			currentAnim = animSet->getAnimation(GLOB_ANIM_RIGHT);
	}
	else if (state == GLOB_STATE_ATTACK)
	{
		if (direction == DIR_DOWN)
			currentAnim = animSet->getAnimation(GLOB_ANIM_ATTACK_DOWN);
		if (direction == DIR_UP)
			currentAnim = animSet->getAnimation(GLOB_ANIM_ATTACK_UP);
		if (direction == DIR_LEFT)
			currentAnim = animSet->getAnimation(GLOB_ANIM_ATTACK_LEFT);
		if (direction == DIR_RIGHT)
			currentAnim = animSet->getAnimation(GLOB_ANIM_ATTACK_RIGHT);
	}
	else if (state == GLOB_STATE_TELEGRAPH)
	{
		if (direction == DIR_DOWN)
			currentAnim = animSet->getAnimation(GLOB_ANIM_TELEGRAPH_DOWN);
		if (direction == DIR_UP)
			currentAnim = animSet->getAnimation(GLOB_ANIM_TELEGRAPH_UP);
		if (direction == DIR_LEFT)
			currentAnim = animSet->getAnimation(GLOB_ANIM_TELEGRAPH_LEFT);
		if (direction == DIR_RIGHT)
			currentAnim = animSet->getAnimation(GLOB_ANIM_TELEGRAPH_RIGHT);
	}
	else if (state == GLOB_STATE_DEAD)
	{
		//always faces the person watching. If directional death, do the same as above
		currentAnim = animSet->getAnimation(GLOB_ANIM_DIE);
	}

	if (resetFrameToBegging)
		currentFrame = currentAnim->getFrame(0);
	else
		currentFrame = currentAnim->getFrame(currentFrame->frameNumber); //change direction for example, wanna change animation, but not what frame we were on

}

void Glob::updateAnimation(){
	if (currentFrame == NULL || currentAnim == NULL)
		return; //cant do much with no frame or no animation

	//if in a moving state, but not actually moving, return to idle 
	if (state == GLOB_STATE_MOVE && !moving){
		changeAnimation(GLOB_STATE_IDLE, true);
	}
	//if should show running animation, lets change the state properly
	if (state != GLOB_STATE_MOVE && moving){
		changeAnimation(GLOB_STATE_MOVE, true);
	}

	frameTimer += TimeController::timerController.dT;
	//time to change frames
	if (frameTimer >= currentFrame->duration)
	{
		//if at the end of the animation
		if (currentFrame->frameNumber == currentAnim->getEndFrameNumber())
		{
			//depends on current animation and whats going on a bit
			if (state == GLOB_STATE_TELEGRAPH){
				//telegraph done, now attack
				attack();
			}
			else if (state == GLOB_STATE_ATTACK){
				//slash done, go back to moving
				changeAnimation(GLOB_STATE_MOVE, true);
			}
			else if (state == GLOB_STATE_DEAD)
			{
				frameTimer = 0;
				//if some how alive again, then change state back to moving
				if (hp > 0)
					changeAnimation(GLOB_STATE_MOVE, true);
				else
					active = false;
			}
			else
			{
				//just reset the animation
				currentFrame = currentAnim->getFrame(0);
			}

		}
		else
		{
			//just move to the next frame in the animation
			currentFrame = currentAnim->getNextFrame(currentFrame);
		}
		frameTimer = 0; //crucial step. If we miss this, the next frame skips real quick
	}
}

void Glob::updateDamages(){
	if (active && hp > 0 && invincibleTimer <= 0){
		for (auto entity = Entity::entities.begin(); entity != Entity::entities.end(); entity++)
		{
			if ((*entity)->active && (*entity)->type == "hero"){
				//reference as livingEntity, so we can access enemies hit box
				LivingEntity* enemy = (LivingEntity*)(*entity);

				if (enemy->damage > 0 && Entity::checkCollision(collisionBox, enemy->hitBox)){

					hp -= enemy->damage;

					if (hp > 0){
						SoundManager::soundManager.playSound("enemyHit");
						invincibleTimer = 0.1;
					}
					//angle from other entity, to towards this entity
					slideAngle = Entity::angleBetweenTwoEntities((*entity), this);
					slideAmount = 300;
				}
			}
		}
	}
}